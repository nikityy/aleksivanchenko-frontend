var showcasePhone = document.querySelector('.showcase__link[itemprop=telephone]');

function reachPhoneCall() {
  window.yaCounter31382438.reachGoal('phoneCall');
}

if (showcasePhone) {
  showcasePhone.addEventListener('click', reachPhoneCall);
}