var APIRequest = require('./request');

function API() {
  throw Error('Use static methods instead');
}

API.article = {};

API.article.create = function(data, callbacks) {
  new APIRequest('/api/article', {
    method: 'POST',
    success: callbacks.success || function() {},
    error: callbacks.error || function() {}
  }).send(data);
};

API.article.update = function(data, callbacks) {
  new APIRequest('/api/article/' + data.id, {
    method: 'PUT',
    success: callbacks.success || function() {},
    error: callbacks.error || function() {}
  }).send(data);
};

API.image = {};

API.image.create = function(formData, callbacks) {
  new APIRequest('/api/image/', {
    method: 'POST',
    contentType: 'application/x-www-form-urlencoded',
    success: callbacks.success || function() {},
    error: callbacks.error || function() {}
  }).send(formData);
};

module.exports = API;
