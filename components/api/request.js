function APIRequest(url, options) {
  this.request = new XMLHttpRequest();

	this.url = url;
  this.contentType = options.contentType || 'application/json';
  this.method = options.method || 'GET';
  this.success = options.success || function() {};
  this.error = options.error || function() {};
	
	if (options.body) {
		this.send(options.body);
	}
}

APIRequest.prototype.onStateChange = function() {
	var request = this.request;
	var readyState = request.readyState;
	
	if (readyState === XMLHttpRequest.DONE) {
		var error = this.error;
		try {
			var response = request.responseText;
			var payload = this.parse(response);
			var status = request.status;
			var hasErrors = !!payload.error;
			
			if (status === 200 && !hasErrors) {
				var success = this.success;
				success(payload);
			} else {
				error(payload);
			}
		}
		catch(e) {
			error(e);
		}
	}
};

APIRequest.prototype.parse = function(response) {
	return JSON.parse(response);
};

APIRequest.prototype.serialize = function(requestBody) {
	return JSON.stringify(requestBody);
};

APIRequest.prototype.send = function(body) {		
	var request = this.request;
	var method = this.method;
	var url = this.url;
  var contentType = this.contentType;
	
	request.open(method, url, true);
	request.onreadystatechange = this.onStateChange.bind(this);	
  
  if (contentType === 'application/json') {
    request.setRequestHeader('Content-Type', contentType);
    request.send(JSON.stringify(body));      
  } else {
    request.send(body);
  }
};

module.exports = APIRequest;
