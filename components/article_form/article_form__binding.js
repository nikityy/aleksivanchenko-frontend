var ArticleForm = require('./article_form');
var API = require('../api/api');
var ImageUploader = require('../image_uploader/image_uploader');

var form = document.querySelector('.article_form');

if (!form) {
  return;
}

var submitButton = document.querySelector('.article_form__create');
var saveButton = document.querySelector('.article_form__save');
var errorMessageBlock = document.querySelector('.article_form__error_message');

if (submitButton) {
  submitButton.addEventListener('click', submit);
}

if (saveButton) {
  saveButton.addEventListener('click', save);
}

var articleForm = new ArticleForm(form);

var imageUploader = document.querySelector('.image_uploader');
var imageUploaderInput = document.querySelector('.image_uploader__input');
var onSend = function(formData) {
  var options = {
    success: function(response) {
      var images = response.images;

      images.forEach(function(image) {
        var markdownImage = '![Alt text](' + image.url +  ')\n';
        articleForm.appendToField('paragraphs', markdownImage);
      });
    },
    error: function(response) {
      errorHandler(
        response && response.error && response.error.code,
        response && response.error && response.error.message
      );
    }
  };
  API.image.create(formData, options);
};

var imageUploader = new ImageUploader(
  imageUploader,
  imageUploaderInput,
  onSend
).initialize();

function submit() {
  var data = articleForm.validateAndGetAll();

  if (!data) {
    return;
  }

  API.article.create(data, {
    success: function(response) {
      var url = response.url;
      window.location = url;
    },
    error: function(response) {
      errorHandler(
        response && response.error && response.error.code,
        response && response.error && response.error.message
      );
    }
  });
}

function save() {
  var data = articleForm.validateAndGetAll();

  if (!data) {
    return;
  }

  API.article.update(data, {
    success: function(response) {
      var url = response.url;
      window.location = url;
    },
    error: function(response, message) {
      errorHandler(
        response && response.error && response.error.code,
        response && response.error && response.error.message
      );
    }
  });
}

function errorHandler(error, errorMessage) {
  switch(error) {
    case 'dom-error':
      setErrorMessage('Внутренняя ошибка. Попробуйте перезагрузить страницу.');
      break;

    case 'validation-error':
      if (errorMessage === 'not-filled') {
        setErrorMessage('Заполнены не все поля.');
      }
      break;

    case 400:
      if (errorMessage === 'URL already exists') {
        setErrorMessage('Запись с таким URL уже существует.');
      } else {
        setErrorMessage('Ошибка в запросе.');
      }
      break;

    default:
      setErrorMessage('Неизвестная ошибка.');
      break;
  }
}

function setErrorMessage(errorMessage) {
  errorMessageBlock.innerHTML = errorMessage;
}

function resetErrorMessage() {
  setErrorMessage('');
}