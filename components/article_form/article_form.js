function ArticleForm(form) {
  this.form = form;
  this.fieldsMeta = {
    id: {
      mapping: 'articleId'
    },
    
    title: {
      mapping: 'articleTitle',
      required: true
    },
    
    url: {
      mapping: 'articleURL',
      required: true
    },
    
    thumbnail: {
      mapping: 'articleThumbnail'
    },

    paragraphs: {
      mapping: 'articleParagraphs',
      required: true
    }
  };
}

ArticleForm.prototype.getAll = function() {
  var form = this.form;
  var meta = this.fieldsMeta;
  var metaKeys = Object.keys(meta);
  
  var hash = {};
  var fieldMeta;
  var formKey;
  metaKeys.forEach(function(metaKey) {
    fieldMeta = meta[metaKey];
    formKey = (fieldMeta.mapping) ? fieldMeta.mapping : metaKey;
    hash[metaKey] = form[formKey].value;
  });
  
  return hash;
};

ArticleForm.prototype.appendToField = function(fieldKey, text) {
  var meta = this.fieldsMeta;
  var metaField = meta[fieldKey];
  
  if (metaField) {
    var formKey = (metaField.mapping) ? metaField.mapping : fieldKey;
    var form = this.form;
    form[formKey].value += text;
  } else {
    throw Error('No field called ' + fieldKey);
  }
};

ArticleForm.prototype.validate = function(hash) {
  this.resetValidationErrors();

  var meta = this.fieldsMeta;
  var metaKeys = Object.keys(meta);
  
  var fieldMeta;
  var validationErrors = metaKeys.map(function(metaKey) {
    fieldMeta = meta[metaKey];
    
    if (fieldMeta.required && !hash[metaKey].trim().length) {
      return Error('required:' + metaKey);
    }
    
    if (fieldMeta.validate && !fieldMeta.validate(hash[metaKey])) {
      return Error('validation-error:' + metaKey);
    }
  }).filter(function(item) {
    return item instanceof Error;
  });
  
  validationErrors.forEach(this.showValidationError.bind(this));
  
  var hasErrors = validationErrors.length;
  return !hasErrors;
};

ArticleForm.prototype.showValidationError = function(error) {
  var errorData = error.message.split(':');
  var errorCode = errorData[0];
  var fieldKey = errorData[1];
  
  var meta = this.fieldsMeta;
  var fieldMeta = meta[fieldKey];
  this.setFieldValidationStatus(fieldMeta, false);
};

ArticleForm.prototype.setFieldValidationStatus = function(fieldMeta, isValid) {
  var form = this.form;
  var formKey = fieldMeta.mapping || fieldKey;
  var node = form.querySelector('[name=' + formKey + ']');
  
  if (isValid) {
    node.classList.remove('is-invalid');
  } else {
    node.classList.add('is-invalid');
  }
};

ArticleForm.prototype.resetValidationErrors = function() {
  var meta = this.fieldsMeta;
  var metaKeys = Object.keys(meta);
  
  var articleForm = this;
  var metaField;
  metaKeys.forEach(function(metaKey) {
    metaField = meta[metaKey];
    articleForm.setFieldValidationStatus(metaField, true);
  });
};

ArticleForm.prototype.validateAndGetAll = function() {
  var hash = this.getAll();
  var isValid = this.validate(hash);
  
  if (isValid) {
    return hash;
  }
};

module.exports = ArticleForm;
