function ImageUploader(form, input, onSend) {
  this.form = form;
  this.wrapper = form;
  this.input = input;
  this.onSend = onSend;

  if (!window.FormData) {
    this.form.classList.add('image_uploader--unsupported');
    throw Error('FormData isn\'t supported');
  }
}

ImageUploader.prototype.initialize = function() {
  var wrapper = this.wrapper;
  wrapper.addEventListener('click', this.onClick.bind(this));
  wrapper.addEventListener('dragover', this.onDragOver.bind(this));
  wrapper.addEventListener('dragleave', this.onDragLeave.bind(this));
  wrapper.addEventListener('drop', this.onDrop.bind(this));

  var input = this.input;
  input.addEventListener('change', this.onChange.bind(this));

  return this;
};

ImageUploader.prototype.onChange = function(e) {
  this.submit();
  return false;
};

ImageUploader.prototype.onClick = function(e) {
  e.stopPropagation();
  e.preventDefault();

  var clickEvent = new CustomEvent('click');
  this.input.dispatchEvent(clickEvent);
};

ImageUploader.prototype.onDragOver = function(e) {
  e.stopPropagation();
  e.preventDefault();

  var wrapper = this.wrapper;
  wrapper.classList.add('drag');
};

ImageUploader.prototype.onDragLeave = function(e) {
  e.stopPropagation();
  e.preventDefault();

  var wrapper = this.wrapper;
  wrapper.classList.remove('drag');
};

ImageUploader.prototype.onDrop = function(e) {
  e.stopPropagation();
  e.preventDefault();

  var wrapper = this.wrapper;
  wrapper.classList.remove('drag');

  this.submit();
};

ImageUploader.prototype.submit = function() {
  var send = this.onSend;

  send(new FormData(this.form));
};

module.exports = ImageUploader;
