module.exports = function(grunt) {
  grunt.initConfig({
    browserify: {
      dist: {
        files: {
          "dist/scripts/built.min.js": ["components/*/*.js"]
        }
      }
    },

    stylus: {
      compile: {
        options: {
          paths: ["components"]
        },
        files: {
          "dist/styles/style.css": ["components/*.styl", "components/*/*.styl"]
        }
      }
    },

    autoprefixer: {
      options: {
        browsers: ["last 2 versions", "iOS 7"]
      },
      no_dest_single: {
        src: "dist/styles/style.css"
      }
    },

    copy: {
      main: {
        files: [
          {
            src: ["images/*"],
            dest: "dist/"
          },
          {
            expand: true,
            cwd: "components",
            src: ["*/*.pug"],
            dest: "dist/views/"
          },
          {
            expand: true,
            src: ["routes/*/*.pug", "routes/*.pug"],
            dest: "dist/views/"
          }
        ]
      }
    },

    watch: {
      styles: {
        files: ["components/*.styl", "components/*/*.styl"],
        tasks: ["stylus", "autoprefixer"]
      },
      js: {
        files: ["components/**/*.js"],
        tasks: ["browserify"]
      }
    }
  });

  grunt.loadNpmTasks("grunt-browserify");
  grunt.loadNpmTasks("grunt-contrib-stylus");
  grunt.loadNpmTasks("grunt-autoprefixer");
  grunt.loadNpmTasks("grunt-contrib-copy");
  grunt.loadNpmTasks("grunt-contrib-watch");

  grunt.registerTask("default", [
    "browserify",
    "stylus",
    "autoprefixer",
    "copy"
  ]);
};
